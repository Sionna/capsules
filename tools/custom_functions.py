import string
from django.utils.text import slugify
import random
# easy_pdf Config
from django.core.files.base import ContentFile


def random_string_generator(size=10, chars=string.ascii_lowercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


def unique_slug_generator(instance, new_slug=None):
    if new_slug is not None:
        slug = new_slug
    else:
        slug = slugify(instance.name)
    klass = instance.__class__
    qs_exists = klass.objects.filter(alias=slug).exists()

    if qs_exists:
        new_slug = "{slug}-{randstr}".format(
            slug=slug, randstr=random_string_generator(size=4))

        return unique_slug_generator(instance, new_slug=new_slug)
    return slug


# -----------  Query -------------------
def find_all(models, data=dict()):
    return models.objects.filter(**data)


def find_all_list_dict(models, data=dict()):
    return models.objects.filter(**data).values()


def find(models, data=dict()):
    try:
        return models.objects.get(**data)
    except models.DoesNotExist:
        return None


def save_instance(models, data=dict()):
    return models.objects.create(**data)

