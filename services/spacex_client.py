import requests


class SpaceXClient:
    base_url = 'https://api.spacexdata.com/v4/'
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}

    def __init__(self, headers=None):
        """
    The __init__ function is called when the class is instantiated.
    It sets up the instance of the class, and takes in any arguments that are passed to it.
    In this case, we're passing in a dictionary of headers.

    :param self: Represent the instance of the class
    :param headers: Set the headers for the request
    :return: A dictionary of headers
    """
        if headers is dict:
            self.headers = headers

    def get_complete_url(self, end_point):
        """
    The get_complete_url function takes in an end_point and returns a complete url.
        Args:
            end_point (str): The endpoint of the API to be appended to the base url.

    :param self: Represent the instance of the class
    :param end_point: Specify the endpoint of the api
    :return: A string that is the base_url plus the end_point
    """
        return "{}{}".format(self.base_url, end_point)

    def set_headers(self, **headers):
        """
    The set_headers function takes in a dictionary of headers and sets the
    headers attribute to that dictionary.


    :param self: Represent the instance of the class
    :param **headers: Pass in a dictionary of headers
    :return: A dictionary of headers
    """
        self.headers = headers

    def get_requester(self, end_point, params={}):
        """
    The get_request function takes in an endpoint and a dictionary of parameters.
    It then returns the response from the request to that endpoint with those parameters.

    :param self: Represent the instance of the class
    :param end_point: Specify the endpoint to be used in the request
    :param params: Pass in a dictionary of parameters to the request
    :return: The result of a get request
    """
        return requests.get(self.get_complete_url(end_point), params=params)

    def get_paginate_requester(self, end_point, data={}):
        """
    The get_request function takes in an endpoint and a dictionary of parameters.
    It then returns the response from the request to that endpoint with those parameters.

    :param self: Represent the instance of the class
    :param end_point: Specify the endpoint to be used in the request
    :param params: Pass in a dictionary of parameters to the request
    :return: The result of a get request
    """
        base_url = self.base_url + end_point + '/query'
        return requests.post(base_url, json=data)
