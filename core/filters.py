from django.db.models import Q
import django_filters
from .models import *


class CapsuleFilter(django_filters.FilterSet):
    q = django_filters.CharFilter(method='search_filter', label="Search")

    class Meta:
        model = Capsule
        fields = ['reuse_count', 'water_landings', 'land_landings', 'last_update',
                  'serial', 'status', 'type',
                  'q']

    @staticmethod
    def search_filter(queryset, name, value):
        return queryset.filter(
            Q(reuse_count__icontains=value) | Q(water_landings__icontains=value) | Q(land_landings__icontains=value)
            | Q(last_update__icontains=value) | Q(serial__icontains=value) | Q(status__icontains=value)
            | Q(type__icontains=value)
        )
