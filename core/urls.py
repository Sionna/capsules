from django.urls import path
from .views import *

app_name = 'core'
urlpatterns = [
    path('api', get_paginate_capsule_api, name="capsule_api"),
    path('', capsule_list, name="capsule_list"),
    path('detail/<str:id>', capsule_detail, name="capsule_detail"),
]
