from django.db import models
from tools.base_model import BaseModel


# https://github.com/r-spacex/SpaceX-API/blob/master/docs/capsules/v4/schema.md
class Capsule(BaseModel):
    STATUS_CHOICES = (
        ('unknown', 'Unknown'),
        ('active', 'Active'),
        ('retired', 'Retired'),
        ('destroyed', 'Destroyed'),
    )
    TYPE_CHOICES = (
        ('Dragon 1.0', 'Dragon 1.0'),
        ('Dragon 1.1', 'Dragon 1.1'),
        ('Dragon 2.0', 'Dragon 2.0'),
    )

    capsule_id = models.CharField(max_length=50, unique=True)
    reuse_count = models.PositiveIntegerField(default=0)
    water_landings = models.PositiveIntegerField(default=0)
    land_landings = models.PositiveIntegerField(default=0)
    last_update = models.CharField(max_length=255, blank=True, null=True)
    serial = models.CharField(max_length=8, unique=True)
    status = models.CharField(choices=STATUS_CHOICES, max_length=20, default='unknown')
    type = models.CharField(choices=TYPE_CHOICES, max_length=50, default='Dragon 1.0')

    def __str__(self):
        return "%s %s" % (self.serial, self.type)

    class Meta:
        indexes = [models.Index(fields=['capsule_id', 'serial'])]

    def launches(self):
        """
    The launches function returns a queryset of all launches associated with the current capsule.
    If there are no launches, it returns None.

    :param self: Represent the instance of the object itself
    :return: A queryset of launches for the given rocket
    :doc-author: Trelent
    """
        queryset = self.launch_set.all()
        return queryset if queryset else None


# https://github.com/r-spacex/SpaceX-API/blob/master/docs/launches/v4/schema.md
class Launch(BaseModel):
    name = models.CharField(max_length=255)
    flight_number = models.PositiveSmallIntegerField(default=1)
    static_fire_date_utc = models.CharField(max_length=255, blank=True, null=True)
    upcoming = models.BooleanField(default=False)
    details = models.TextField(blank=True, null=True)
    link_patch_image = models.URLField()
    link_patch_webcast = models.URLField()
    success = models.BooleanField()
    launch_id = models.CharField(max_length=50, unique=True)
    capsule = models.ForeignKey(Capsule, on_delete=models.CASCADE)

    def __str__(self):
        return "%s %s" % (self.name, self.flight_number)

    class Meta:
        indexes = [models.Index(fields=['launch_id', 'name', 'flight_number'])]
