from django.http import JsonResponse
from django.shortcuts import redirect, render

from core.filters import CapsuleFilter
from core.models import Capsule, Launch
from core.serializers import CapsuleSerializer

from tools.custom_functions import find, find_all, save_instance
from django.contrib import messages
from services.spacex_client import SpaceXClient


def get_paginate_capsule_api(request):
    """
The get_capsule_api function is used to get the list of capsules from SpaceX API.
    It will save all data in Sqlite database and display it on a page.

:param request: Get the request object
:return: A list of capsules
"""
    limit = 10
    offset = 0
    total = 0
    data = []
    params = {}
    end_point = 'capsules'

    http_client = SpaceXClient()
    response_data = http_client.get_paginate_requester(end_point, params)
    response_data_json = response_data.json()

    if response_data_json:
        limit = response_data_json['limit']
        offset = response_data_json['offset']
        total = response_data_json['totalDocs']
        data = response_data_json['docs']

    while offset < total:
        for caps in data:
            capsule = find(Capsule, {'capsule_id': caps['id']})
            if capsule is None:
                capsule = save_instance(Capsule, {
                    'capsule_id': caps['id'],
                    'reuse_count': caps['reuse_count'],
                    'water_landings': caps['water_landings'],
                    'land_landings': caps['land_landings'],
                    'last_update': caps['last_update'],
                    'serial': caps['serial'],
                    'status': caps['status'],
                    'type': caps['type']
                })

            for launch_id in caps['launches']:
                # list of launches attach to capsule
                end_point_launche = 'launches/' + launch_id
                launches = http_client.get_requester(end_point_launche, params)

                launcheJson = launches.json()
                if launcheJson:

                    launch = find(Launch, {'launch_id': launch_id})
                    if launch is None:
                        # save launch
                        save_instance(Launch, {
                            'launch_id': launch_id,
                            'success': bool(launcheJson['success']),
                            'upcoming': bool(launcheJson['upcoming']),
                            'name': launcheJson['name'],
                            'flight_number': launcheJson['flight_number'],
                            'details': launcheJson['details'],
                            'static_fire_date_utc': launcheJson['static_fire_date_utc'],
                            'link_patch_image': launcheJson['links']['patch']['small'],
                            'link_patch_webcast': launcheJson['links']['webcast'],
                            'capsule': capsule
                        })
        offset += limit
        params = {
            'options': {
                'limit': limit,
                'offset': offset
            }
        }

        response_data = http_client.get_paginate_requester(end_point, params)
        response_data_json = response_data.json()

        if response_data_json:
            data = response_data_json['docs']

    messages.success(request, "Recuperation des capsules réalisée avec succès.")

    template_name = "core/api_data.html"
    context = {
        'page': {
            'title': 'Capsule',
            'name': 'Capsule'
        },
        'charge_active': True,
        'capsules': data
    }
    return render(request, template_name, context)


def capsule_list(request):
    """
The capsule_list function is used to get the list of capsules from database.
    It will fetch capsule data from local database and display it on a page.

:param request: Get the request object
:return: A list of capsules
"""
    request_params = request.GET.dict()
    limit = int(request_params.get('limit', -1))
    offset = int(request_params.get('offset', 0))

    sort = request_params.get('sort')
    direction = request_params.get('dir', 'asc')

    order_by = None
    if sort is not None:
        order_by = sort if direction == 'asc' else '-' + sort

    queryset = find_all(Capsule, {})
    filters = CapsuleFilter(request.GET, queryset=queryset)

    if order_by is not None:
        query_set = filters.qs.order_by(order_by) if int(limit) < 0 else filters.qs.order_by(order_by)[
                                                                         offset:offset + limit]
    else:
        query_set = filters.qs if int(limit) < 0 else filters.qs[offset:offset + limit]

    is_xml_http_request = request.headers.get('X-Requested-With') == 'XMLHttpRequest'

    if is_xml_http_request:
        serializer = CapsuleSerializer(query_set, many=True)
        return JsonResponse({
            'code': 1,
            'msg': 'success',
            'data': serializer.data,
            'total': filters.qs.count(),
        })

    template_name = "core/index.html"
    context = {
        'page': {
            'title': 'Liste des capsules',
            'name': 'Capsules'
        },
        'list_active': True,
        'capsules': query_set
    }
    return render(request, template_name, context)


def capsule_detail(request, id):
    """
The capsule_detail function is used to get one capsule data from database.
    It will fetch capsule data from local database and display it on a page.

:param request: Get the request object
:param id: Get a capsule id
:return: One capsule object for view details
"""

    capsule = find(Capsule, {'capsule_id': id})
    if capsule is None:
        messages.error(request, "Capsule introuvable.")
        return redirect('capsule:capsule_list')

    template_name = "core/detail.html"
    context = {
        'page': {
            'title': 'Detail capsule',
            'name': 'Detail capsule'
        },
        'list_active': True,
        'capsule': capsule
    }
    return render(request, template_name, context)
