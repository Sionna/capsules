from django import forms

from core.models import Capsule, Launch


class CapsuleForm(forms.ModelForm):

    class Meta:
        model = Capsule
        fields = ['capsule_id', 'reuse_count', 'water_landings', 'land_landings', 'last_update', 'serial',
                  'status', 'type']


class LauncheForm(forms.ModelForm):
    class Meta:
        model = Launch
        fields = ['name', 'flight_number', 'static_fire_date_utc', 'upcoming', 'details',
                  'link_patch_image', 'link_patch_webcast', 'success', 'launch_id', 'capsule']
