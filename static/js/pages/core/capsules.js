/*
 * This file is part of the capsules project
 * Copyright (c) 2023 OAS
 * @author Sionna Ouattara <sionnaouattara@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

var cols = [{
    field: 'id',
    title: 'Id'
}, {
    field: 'serial',
    title: 'Serial'
}, {
    field: 'type',
    title: 'Type'
}, {
    field: 'water_landings',
    title: 'Water landings',
    align:'center'
}, {
    field: 'land_landings',
    title: 'Land landings',
    align:'center'
}, {
    field: 'status',
    title: 'Status'
},{
    field: '',
    title: 'Actions',
    align: 'center',
    formatter: rowActionFormatter
}];

function rowActionFormatter (value, row) {
    let linkOption = [];
    let itemName = row.number;

    let detailUrl = '/detail/' + row.capsule_id;
    let detailText = 'Voir plus';
    let html = '\t<a class="btn btn-primary btn-sm" href="' + detailUrl + '" data-toggle="tooltip" data-placement="left" ' +
        'data-original-title="' + utils.stripHtml(detailText) + '">' + detailText +
        '</a>\n';



    return html;
}


$(document).ready(function () {
    var $dataTable = $("#data-table");
    var urlTable = $dataTable.data('url');

    var uri = new URI(urlTable);
    var dataUrl = uri.pathname() + uri.search();

    var bootstrapTableCls = new BaseBootstrapTable($dataTable, dataUrl, cols, '');
     bootstrapTableCls.refreshOptions({
        search: true,
    });
    bootstrapTableCls.addToolbar('#toolbar');
    bootstrapTableCls.refreshOptions({
        ajaxOptions: {
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest')
                xhr.setRequestHeader('X-CSRFToken', csrftoken)
            },
            responseHandler: function (response) {
                response.data = JSON.parse(response.data);
                return response;
            }
        },
        onClickCell: function(field, value, row, $element) {
            console.log(field,'field')
        },
        onEditableSave: function(field, value, row, $element) {
            console.log(value,'value')
        }

    });
    bootstrapTableCls.init();
});