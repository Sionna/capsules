/*
 * This file is part of the capsules project
 * Copyright (c) 2023 OAS
 * @author Sionna Ouattara <sionnaouattara@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


window.utils = {
    capitalizeFirstLetter: function (string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    },
    capitalizeFirstLetterOfEachWord: function (string) {
        var str = string.toLowerCase()
        return str.replace(/\w\S*/g, function (txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        });
    },
    loadTemplate: function (views, callback) {
        var deferreds = [];

        $.each(views, function (index, view) {
            $.get('js/templates/' + view + '.html', function (data) {
                _.template(data);
                deferreds.push(view);
            });
        });

        $.when.apply(null, deferreds).done(callback);
    },
    launchModal: function (view, title, size) {
        $('#modal-container .modal-body').html(view);
        $('#modal-container .modal-header h5').html(title);
        $("#modal-container").modal({
            backdrop: 'static',
            keyboard: false
        });
        var allowedSizes = ['lg', 'md', 'sm'];
        if (typeof size !== 'undefined' && allowedSizes.indexOf(size) !== -1) {
            $("#modal-container .modal-dialog").addClass('modal-' + size);
        }
    },
    launchModalAsIt: function (title) {
        $('#modal-container .modal-header h5').html(title);
        $("#modal-container").modal({
            backdrop: 'static',
            keyboard: false
        });
    },
    displayValidationErrors: function (messages) {
        for (var key in messages) {
            if (messages.hasOwnProperty(key)) {
                this.addValidationError(key, messages[key]);
            }
        }
        this.showAlert('Warning!', 'Fix validation errors and try again', 'alert-warning');
    },
    launchNotification: function (title, text, klass) {
        var opts = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": 300,
            "hideDuration": 1000,
            "timeOut": 5000,
            "extendedTimeOut": 1000,
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "escapeHtml": false
        };

        toastr.options = opts;
        var notification;
        switch (klass) {
            case 'success':
                notification = toastr.success;
                break;
            case 'error':
                notification = toastr.error;
                break;
            case 'warning':
                notification = toastr.warning;
                break;
            default:
                notification = toastr.info;
        }
        notification(text, title);
    },
    addValidationError: function (field, message) {
        var controlGroup = $('#' + field).parent().parent();
        controlGroup.addClass('error');
        $('.help-inline', controlGroup).html(message);
    },
    removeValidationError: function (field) {
        var controlGroup = $('#' + field).parent().parent();
        controlGroup.removeClass('error');
        $('.help-inline', controlGroup).html('');
    },
    showAlert: function (title, text, klass) {
        text = typeof text !== 'undefined' ? text : '';
        klass = typeof klass !== 'undefined' ? klass : 'alert-info';
        $('.alert').removeClass("alert-error alert-warning alert-success alert-info");
        $('.alert').addClass(klass);
        $('.alert .alert-content').html('<strong>' + title + '</strong> ' + text);
        $('.alert').removeClass('hide');
    },
    hideAlert: function () {
        $('.alert').addClass('hide');
    },
    blinkTitle: function (newTitle) {
        window.myTitle = document.title;
        var title = window.myTitle;

        window.myTimer = window.setInterval(function () {
            document.title = document.title == title ? newTitle : title;
        }, 4000);
    },
    stopBlinkTitle: function () {
        clearInterval(window.myTimer);
    },
    getCookie: function (name) {
        var pos = document.cookie.indexOf(name + "=");
        if (pos === -1) {
            return null;
        } else {
            var pos2 = document.cookie.indexOf(";", pos);
            if (pos2 === -1) {
                return unescape(
                    document.cookie.substring(
                        pos + name.length + 1));
            } else {
                return unescape(
                    document.cookie.substring(
                        pos + name.length + 1, pos2));
            }
        }
    },
    resetHelperPlugin: function () {
        $("[rel*=popover]").popover();

        $("a[rel*=tooltip]").tooltip({
            placement: 'top'
        });
    },
    getPictureThumbUrl: function (input) {
        var uri = URI(input);
        var inputPath = uri.path();
        var filename = inputPath.substr(0, inputPath.lastIndexOf('.')) || inputPath;
        var ext = inputPath.split('.').pop();
        return filename + '_thumb.' + ext;
    },
    // proxy launchNotification
    notification: function (aTitle, aMsg, aType) {
        utils.launchNotification(aTitle, aMsg, aType);
    },
    filteredInput: function () {
        var uri = new URI(window.location.href);
        var qs = uri.query(true);
        var tpl = '<span class="tag badge badge-secondary">\n\t<span></span>\n\t' +
            '<a href="#" class="text-light" title="Retirer ce filtre">' +
            '<i class="remove fas fa-times-circle"></i></a>\n</span>';
        var filterForm = $('.form-filter');
        var placeholder = $('.filters-placeholder');
        placeholder.html('');
        $.each(qs, function (index, value) {
            if (value === '') {
                return true;
            }
            var input = $('.form-filter #' + index);
            if (input.length === 0) {
                return true;
            }

            var selectedText = input.data('placeholder') + ': ';
            switch (input.prop("type")) {
                case 'text':
                case 'number':
                    selectedText += input.val();
                    break;
                case 'select-one':
                    selectedText += input.find('option[value="' + value + '"]').text();
                    break;
                case 'radio':
                    selectedText += input.val();
                    break;
            }

            //var selectedText = input.find('option[value="' + value + '"]').text();
            var item = $(tpl);
            item.attr('data-filter', index).find('span').text(selectedText);
            placeholder.append(item);
        });
    },
    numberFormat: function (number, decimals, decPoint, thousandsSep) {
        decimals = decimals || 0;
        number = parseFloat(number);

        if (!decPoint || !thousandsSep) {
            decPoint = '.';
            thousandsSep = ',';
        }

        var roundedNumber = Math.round(Math.abs(number) * ('1e' + decimals)) + '';
        var numbersString = decimals ? roundedNumber.slice(0, decimals * -1) : roundedNumber;
        var decimalsString = decimals ? roundedNumber.slice(decimals * -1) : '';
        var formattedNumber = "";

        while (numbersString.length > 3) {
            formattedNumber += thousandsSep + numbersString.slice(-3);
            numbersString = numbersString.slice(0, -3);
        }

        return (number < 0 ? '-' : '') + numbersString + formattedNumber +
            (decimalsString ? (decPoint + decimalsString) : '');
    },
    formWarning: function (formSelector, allowHidden) {
        if (typeof formSelector === 'undefined') {
            return false;
        }

        if (!(formSelector instanceof jQuery)) {
            formSelector = $(formSelector);
        }

        var $inputs = formSelector.find("input, select, textarea");
        var formChanged = false;
        var submited = false;

        $inputs.filter('[required]').each(function (i, item) {
            $(this).parents('.form-group').find('label').append(' <span class="text-danger">*</span>');
        });

        if ((typeof $inputs.attr("required") !== 'undefined') && $inputs.attr("required").length > 0) {
            var msg = 'Les champs marqués (<span class="text-danger">*</span>) sont obligatoires';
            utils.launchNotification('Formulaire', msg, 'warning');
        }

        $inputs.on('change', function (e) {
            formChanged = true;
        });

        formSelector.submit(function () {
            submited = true;
        });

        window.onbeforeunload = function () {
            if (formChanged && !submited) {
                return "Etes-vous sûr de vouloir quitter cette page? " +
                    "Des données non enregistrées seront perdues.";
            }
        };
    },
    randomColorGenerator: function () {
        return '#' + ('000000' + Math.floor(Math.random() * 16777215).toString(16)).slice(-6);
    },
    stripHtml: function (html) {
        var doc = new DOMParser().parseFromString(html, 'text/html');
        return doc.body.textContent || "";
    },
    truncate: function (string, limit) {
        if (string.length > limit) {
            return string.substring(0, limit) + "...";
        }
        else {
            return string;
        }
    }
};

$(function () {

    $("[rel*=popover]").popover();

    $(document.body).tooltip({
        selector: '[data-toggle="tooltip"]'
    });

    $(".content-header .btn-xs.pull-right").tooltip({
        title: 'Ajouter',
        placement: 'left'
    });

    $('#modal-container').on('shown.bs.modal', function (e) {
        utils.resetHelperPlugin();
    });

    $('.alert').on('close.bs.alert', function (e) {
        e.preventDefault();
        $(this).addClass('hide');
    });
    $('#shortcut-toggler, #notification-toggler, .sidebar-toggle').tooltip();

});

/**
 *
 * @param selector
 * @param ajaxUrl
 * @param cols
 * @param actionsHtml
 * @param checkbox
 * @constructor
 */
var BaseBootstrapTable = function (selector, ajaxUrl, cols, actionsHtml, checkbox) {
    var currentURI = new URI(window.location.href);
    var currentURISearchObj = currentURI.search(true);

    var ajaxUrlURI = new URI(ajaxUrl);
    var ajaxUrlURISearchObj = ajaxUrlURI.search(true);
    var searchObj = Object.assign(currentURISearchObj, ajaxUrlURISearchObj);
    ajaxUrlURI.search(searchObj);

    this.selector = selector;
    this.url = ajaxUrl;
    this.cols = cols;
    this.options = {
        locale: 'fr-FR',
        striped: true,
        pagination: true,
        selectItemName: 'btnSelectItem',
        search: false,
        showSearchButton: true,
        showColumns: true,
        showRefresh: true,
        sidePagination: 'server',
        pageSize: 20,
        cache: false,
        showToggle: true,
        pageList: [10, 25, 50, 100, 'all'],
        dataField: 'data',
        idField: 'id',
        uniqueId: 'id',
        mobileResponsive: true,
        method: 'get',
        url: ajaxUrlURI.toString(),
        showExport: true,
        exportTypes: "['excel', 'csv', 'pdf']",
        exportOptions: {
            fileName: "stelab",
            worksheetName: "stelab1",
            jspdf: {
                orientation: 'l',
                margins: { left: 20, top: 10 },
                autotable: {
                    beforePageContent: function (data) {
                        var doc = this.tableExport.doc;
                        doc.text("Header", 40, 30);
                    },
                    afterPageContent: function (data) {
                        var doc = this.tableExport.doc;
                        var str = "Page " + data.pageCount;
                        // Total page number plugin only available in jspdf v1.0+
                        if (typeof doc.putTotalPages === 'function') {
                            str = str + " / " + doc.internal.getNumberOfPages();
                        }
                        doc.text(str, data.settings.margin.left, doc.internal.pageSize.height - 30);
                    },
                    styles: {
                        rowHeight: 20,
                        fontSize: 10
                    },
                    headerStyles: {
                        fillColor: 255,
                        textColor: 0
                    },
                    alternateRowStyles: {
                        fillColor: [204, 204, 204],
                        textColor: 255
                    }
                }
            }
        },
        clickToSelect: true,
        columns: cols,
        rowAttributes: function (row) {
            return {
                'data-id': row.id
            };
        },
        queryParams: function (params) {
            if (typeof params.sort !== 'undefined') {
                params.dir = params.order;
            }

            if (typeof params.search !== 'undefined') {
                params.q = params.search;
                delete params.search;
            }

            delete params.order;
            return params;
        },
    };

    if (actionsHtml === false || actionsHtml === '') {
        this.actionsHtml = '';
    } else if (actionsHtml === 'undefined' || actionsHtml === undefined) {
        var defaultHtml = '<a role="button" class="btn btn-outline-warning btn-sm" ' +
            'id="btn-edit" data-toggle="tooltip" data-placement="top" title="Modifier">' +
            '<i class="fas fa-pencil-alt"></i></a>\n ' +
            '<a role="button" class="btn btn-outline-danger btn-sm" ' +
            'id="btn-delete" data-toggle="tooltip" data-placement="top" title="Supprimer">' +
            '<i class="fas fa-times"></i></a>\n';
        this.actionsHtml = defaultHtml;
    } else {
        this.actionsHtml = actionsHtml;
    }

    this.checkbox = !!parseInt(checkbox);
};

/**
 *
 * @param callback
 */
BaseBootstrapTable.prototype.init = function (callback) {
    var self = this;
    if (!(self.selector instanceof jQuery)) {
        this.selector = $(this.selector);
    }

    $.each(self.cols, function (i, val) {
        if (val.field === '') {
            val.sortable = false;
        } else if (typeof val.sortable === 'undefined') {
            val.sortable = true;
        }

        if (val.field === 'id') {
            val.visible = false;
        }

    });

    if (self.checkbox) {
        self.cols.unshift({
            field: 'state',
            checkbox: true
        });
    }

    if (self.actionsHtml !== '') {
        self.cols.push({
            field: '',
            title: '',
            sortable: false,
            formatter: function () {
                return self.actionsHtml;
            }
        });
    }

    self.options.cols = self.cols;
    self.selector.bootstrapTable(self.options);

    if (callback) {
        callback();
    }
};

/**
 *
 * @param useCurrentPage
 * @param callback
 */
BaseBootstrapTable.prototype.getData = function (useCurrentPage, callback) {
    if (!(this.selector instanceof jQuery)) {
        this.selector = $(this.selector);
    }
    var data = this.selector.bootstrapTable('getData', useCurrentPage || false);

    if (callback) {
        callback(data);
    }

    return data;
};

/**
 *
 * @param id
 * @param callback
 */
BaseBootstrapTable.prototype.getRow = function (id, callback) {
    if (!(this.selector instanceof jQuery)) {
        this.selector = $(this.selector);
    }
    var row = this.selector.bootstrapTable('getRowByUniqueId', id);

    if (callback) {
        callback(row);
    }

    return row;
};

/**
 *
 * @param data
 * @param callback
 */
BaseBootstrapTable.prototype.appendRow = function (data, callback) {
    var fields = _.pluck(this.cols, 'field');
    var row = {};

    _.each(data, function (val, i) {
        if (_.indexOf(fields, i) !== -1) {
            row[i] = val;
        }
    });

    if (!(this.selector instanceof jQuery)) {
        this.selector = $(this.selector);
    }
    this.selector.bootstrapTable('append', row);

    if (callback) {
        callback();
    }
};

/**
 *
 * @param data
 * @param callback
 */
BaseBootstrapTable.prototype.editRow = function (data, callback) {
    var fields = _.pluck(this.cols, 'field');
    var row = {};

    _.each(data, function (val, i) {
        if (_.indexOf(fields, i) !== -1) {
            row[i] = val;
        }
    });

    if (!(this.selector instanceof jQuery)) {
        this.selector = $(this.selector);
    }
    var params = {
        id: data.id,
        row: data
    };
    this.selector.bootstrapTable('updateByUniqueId', params);

    if (callback) {
        callback();
    }
};

/**
 *
 * @param id
 * @param callback
 */
BaseBootstrapTable.prototype.removeRow = function (id, callback) {
    if (!(this.selector instanceof jQuery)) {
        this.selector = $(this.selector);
    }
    this.selector.bootstrapTable('removeByUniqueId', id);

    if (callback) {
        callback();
    }
};

/**
 *
 * @param newUrl
 * @param callback
 */
BaseBootstrapTable.prototype.refresh = function (newUrl, callback) {
    var self = this;

    if (!(self.selector instanceof jQuery)) {
        self.selector = $(self.selector);
    }

    var options = {};
    if (typeof newUrl !== 'undefined') {
        options.url = newUrl;
        self.options.url = newUrl;
    }
    self.selector.bootstrapTable('refresh', options);

    if (typeof callback !== 'undefined') {
        callback();
    }
};

BaseBootstrapTable.prototype.getOptions = function () {
    return this.selector.bootstrapTable('getOptions');
};

/**
 *
 * @param options
 * @param callback
 */
BaseBootstrapTable.prototype.refreshOptions = function (options, callback) {
    var self = this;

    if (!(self.selector instanceof jQuery)) {
        self.selector = $(self.selector);
    }

    var newOptions = Object.assign(self.options, options);
    self.selector.bootstrapTable('refreshOptions', newOptions);

    if (typeof callback !== 'undefined') {
        callback();
    }
};

/**
 *
 * @param callback
 */
BaseBootstrapTable.prototype.toggleView = function (callback) {
    this.selector.bootstrapTable('toggleView');

    if (typeof callback !== 'undefined') {
        callback();
    }
};

/**
 *
 * @param callback
 */
BaseBootstrapTable.prototype.appliedRowStyle = function (callback) {
    this.options.striped = false;
    this.options.rowStyle = callback;
};

/**
 * @param callback
 */
BaseBootstrapTable.prototype.addToolbar = function (toolbarSelector) {
    if (toolbarSelector !== 'undefined') {
        this.options.toolbar = toolbarSelector;
    }
};

/**
 * @param callback
 */
BaseBootstrapTable.prototype.destroy = function () {
    this.selector.bootstrapTable('destroy');
};

/**
 * @param expander
 */
BaseBootstrapTable.prototype.expanderRow = function (expander) {
    var self = this;
    if ($.isFunction(expander)) {
        self.options.detailView = true;
        self.options.onExpandRow = expander;
    }
};

BaseBootstrapTable.prototype.applyMethod = function (method, parameter) {
    return this.selector.bootstrapTable(method, parameter);
};

BaseBootstrapTable.prototype.applyEvent = function (eventName, $function) {
    var self = this;
    if ($.isFunction($function)) {
        var applyEvent = {};
        applyEvent[eventName] = $function;
        self.selector.bootstrapTable(applyEvent);
    }
};

$(function () {
    "use strict";

    $("#btn-add").on('click', function (e) {
        utils.formWarning($("#form-entity"));
        e.stopPropagation();
    });

    $(document).on('submit', '#form-entity', function (e) {
        $(this).find('button[type="submit"]').prop('disabled', true);
    });

    utils.filteredInput();

    $(document.body).on('click', '.filters-placeholder a', function (e) {
        e.preventDefault();
        var filterQs = $(this).parents('span').data('filter');
        var uri = new URI(window.location.href);
        uri.removeSearch(filterQs);
        var newUrl = uri.filename() + uri.search();
        var stateObj = {
            link: uri.filename()
        };
        History.pushState(stateObj, null, newUrl);
    });

    $(".form-filter").on('submit', function (e) {
        e.preventDefault();

        var formUrl = $(this).prop('action');
        var formGetUrl = formUrl + '?' + $(this).serialize();
        var uri = new URI(formGetUrl);
        uri.normalizeSearch();
        var newUrl = uri.filename() + uri.search();
        var stateObj = {
            link: uri.filename()
        };
        History.pushState(stateObj, null, newUrl);
    });

    History.Adapter.bind(window, 'statechange', function () {
        var State = History.getState();
        var $documentTable = $("#data-table");
        if ($documentTable.length) {
            var dataUrl;
            if (typeof State.cleanUrl !== 'undefined') {
                dataUrl = State.cleanUrl;
            } else {
                var uri = new URI(window.location.href);
                dataUrl = uri.pathname() + uri.search();
            }

            var bootstrapTableCls = new BaseBootstrapTable($documentTable, dataUrl);
            bootstrapTableCls.refresh(dataUrl);
        }
        utils.filteredInput();
    });

    $("#clear-filters").on('click', function (e) {
        e.preventDefault();
        var uri = new URI(window.location.href);
        if (uri.search() !== '') {
            var stateObj = {
                link: uri.filename()
            };
            History.pushState(stateObj, null, uri.filename());
            utils.filteredInput();
            $(".form-filter").trigger("reset");
        } else {
            utils.launchNotification('<i class="fa fa-filter"></i> Filtre',
                ':( Aucun filtre n\'a été appliqué!', 'warning');
        }
    });

    var $alertCtn = $(".alert .alert-content");
    if ($.trim($alertCtn.html()) !== '') {
        $alertCtn.parent('.alert').removeClass('hide');
    }

    $(document.body).on('click', "input#checkAll", function (e) {
        $('input:checkbox').not(this).prop('checked', this.checked);
    });

    $("a[href^='http:']:not([href^='http://" + window.location.host + "'])").each(function () {
        $(this).attr("target", "_blank");
        $(this).attr("rel", "nofollow");
    });

    var $form = $("form").not('.navbar-form, .form-filter');
    utils.formWarning($form);

});

Number.prototype.toDecimal = function (precision) {
    var dec = 10;
    return Math.round(this * (Math.pow(dec, precision))) / (Math.pow(dec, precision));
};

function parseDotNotation(str, val, obj) {
    var currentObj = obj;
    var keys = str.split(".");
    var l = Math.max(1, keys.length - 1);

    for (var i = 0; i < l; ++i) {
        var key = keys[i];
        currentObj[key] = currentObj[key] || {};
        currentObj = currentObj[key];
    }

    currentObj[keys[i]] = val;
    delete obj[str];
}

Object.expand = function (obj) {
    for (var key in obj) {
        if (key.indexOf(".") !== -1) {
            parseDotNotation(key, obj[key], obj);
        }
    }
    return obj;
};

String.prototype.removeAccent = function () {
    var accent = [
        /[\300-\306]/g, /[\340-\346]/g, // A, a
        /[\310-\313]/g, /[\350-\353]/g, // E, e
        /[\314-\317]/g, /[\354-\357]/g, // I, i
        /[\322-\330]/g, /[\362-\370]/g, // O, o
        /[\331-\334]/g, /[\371-\374]/g, // U, u
        /[\321]/g, /[\361]/g, // N, n
        /[\307]/g, /[\347]/g, // C, c
    ];
    var noaccent = ['A', 'a', 'E', 'e', 'I', 'i', 'O', 'o', 'U', 'u', 'N', 'n', 'C', 'c'];

    var str = this;
    for (var i = 0; i < accent.length; i++) {
        str = str.replace(accent[i], noaccent[i]);
    }

    return str;
}

String.prototype.isUpperCase = function () {
    return this.valueOf().toUpperCase() === this.valueOf();
};
